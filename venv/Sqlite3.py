import sqlite3
import time

class tables():

    def list_tables(self,conn):
        cursor = conn.cursor()
        cursor.execute('SELECT name from sqlite_master where type= "table"')
        print (cursor.fetchall())


    def insert_log(self,conn):
        path = "/home/carlos1991/Desktop/SYSLOG/"
        file = input("Introduce el nombre del archivo: ")
        l = conn.cursor()
        l.execute("CREATE TABLE IF NOT EXISTS LOG(ID_DEV integer, DATE text, LOG text)")
        l.execute("SELECT * FROM DEVICES")
        dev = (l.fetchall())
        with open (file=(str(path) + str(file)),mode="r") as f:
            while True:
                line = f.readline()
                if not line:
                    break
                dev_split, log_split = line.split(sep=" : ", maxsplit=1)
                month, day, local_time, host, ip = dev_split.split(maxsplit=4)
                date = str(month) + str(day) + str(local_time)
                for x in dev:
                    if str(host) in str(x[1]):
                        id_dev = str(x[0])
                l.execute("INSERT INTO LOG (ID_DEV,DATE,LOG) VALUES (?,?,?)", (id_dev,date,log_split))
        conn.commit()


    def device_list (self,conn):
        c = conn.cursor()
        file = "/home/carlos1991/Desktop/device_list.txt"
        with open (file,"r") as f:
            while True:
                line = f.readline()
                if not line:
                    break
                item1,item2 = line.split()
                c.execute("CREATE TABLE IF NOT EXISTS DEVICES(ID_DEV integer primary key autoincrement, NAME_DEV text, IP_DEV text)")
                c.execute("INSERT INTO DEVICES (NAME_DEV,IP_DEV) VALUES (?,?)", (item2, item1))
            conn.commit()

    def list_test(self,conn):
            with open("/home/carlos1991/Desktop/SYSLOG/edited_syslog_info.07.txt", "r") as s:
                host = []
                while True:
                    linea = s.readline()
                    if not linea:
                        break

                    dev_split, log_split = linea.split(sep=" : ",maxsplit=1)
                    month, day, local_time, host, ip= dev_split.split(maxsplit=4)
                    date = str(month) + str(day) + str(local_time)
                    print (linea)


                cursor = conn.cursor()
                cursor.execute("SELECT * FROM DEVICES")
                dev_list = cursor.fetchall()

    def delete1(self):
        path = "/home/carlos1991/Desktop/SYSLOG/"
        old = input("Introduce la ruta del log_file: ")
        new = "/home/carlos1991/Desktop/SYSLOG/edited_" + str(old) + ".txt"
        old_file2 = str(path) + str(old)
        errors = (
            "CEM SAP Packet Errors",
            "TCP MD5 digest match Failure",
            "[adminSAM]",
            "dsxClockSyncStateChange",
            "sonetSDHChannelAlarm",
            "PPPOE",
            "scriptLD",
            "msapCreationFailure",
            "adiusInetServerOperStatusChange",
            "radiusOperStatusChange",
            "tacplusInetSrvrOperStatusChange"
        )
        matches = (
            " ASR",
            " ASE"
        )
        with open(file=new, mode="a") as txt:
            with open(file=old_file2, mode="r") as f:
                counter_delete = 0
                counter_accept = 0
                while True:
                    linea = f.readline()
                    flag = 0
                    for x in errors:
                        if x in linea:
                            print("ERROR")
                            flag += 1
                            counter_delete += 1
                    if flag == 0:
                        for y in matches:
                            if y in linea:
                                print("LINEA OK")
                                flag = 50
                    if flag == 50:
                        txt.write(linea)
                        counter_accept += 1
                    if not linea:
                        break
        print("Lineas borradas = " + str(counter_delete))
        print("Lineas guardadas = " + str(counter_accept))



if __name__ == "__main__":
    start_time = time.time()
    sql_path = "/home/carlos1991/Desktop/SQlite/Dashboard.db"
    connection = sqlite3.connect(sql_path)
    table = tables()
    case = int(input("1 - Listado Tablas.\n2 - Cargar logs desde archivo.\n3 - Listar Equipos\n4 - Depurar archivo de log\n>"))

    if case == 1:
        table.list_tables(connection)
    if case == 3:
        table.insert_log(connection)
    if case == 2:
        table.device_list(connection)
    if case == 4:
        table.delete1()

    connection.close()
    print("time elapsed: {:.2f}s".format(time.time() - start_time))




