import time

def delete1(errors,old,new,matches):
    txt = open (file=new,mode="a")
    with open(file=old,mode="r") as f:
        counter_delete = 0
        counter_accept = 0
        while True:
            linea = f.readline()
            flag = 0
            for x in errors:
                if x in linea:
                    print("ERROR")
                    flag += 1
                    counter_delete +=1
            if flag == 0:
                for y in matches:
                    if y in linea:
                        print ("LINEA OK")
                        flag = 50
            if flag == 50:
                txt.write(linea)
                counter_accept += 1
            if not linea:
                break
    print ("Lineas borradas = " + str(counter_delete))
    print ("Lineas guardadas = " + str(counter_accept))

if __name__ == "__main__":
    start_time = time.time()
    new_file= "/home/carlos1991/Desktop/syslog_text_depurado.txt"
    old_file = "/home/carlos1991/Desktop/syslog_info.31"
    errors = (
        "CEM SAP Packet Errors",
        "TCP MD5 digest match Failure",
        "[adminSAM]",
        "dsxClockSyncStateChange"
    )
    matches = (
        "ASR",
        "ASE",
        "GASR1"
    )
    delete1(errors,old_file,new_file,matches)
    print("time elapsed: {:.2f}s".format(time.time() - start_time))
    print ("SUBIR A GITLAB")


